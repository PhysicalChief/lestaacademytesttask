// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "Engine/Classes/Components/BoxComponent.h"

AStandartPlatformWithOverlapBox::AStandartPlatformWithOverlapBox()
{
	PrimaryActorTick.bCanEverTick = true;

	boxOverlap = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxOverlapCharacter"));
	boxOverlap->AttachTo(platformMeshComponent);
	boxOverlap->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	boxOverlap->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	boxOverlap->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

void AStandartPlatformWithOverlapBox::BeginPlay()
{
	Super::BeginPlay();

	boxOverlap->OnComponentBeginOverlap.AddDynamic(this, &AStandartPlatformWithOverlapBox::OnBoxBeginOverlap);
	boxOverlap->OnComponentEndOverlap.AddDynamic(this, &AStandartPlatformWithOverlapBox::OnBoxEndOverlap);
}

void AStandartPlatformWithOverlapBox::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AStandartPlatformWithOverlapBox::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
}

void AStandartPlatformWithOverlapBox::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}
