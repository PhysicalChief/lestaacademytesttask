// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatform.h"
#include "RotatablePlatform.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API ARotatablePlatform : public AStandartPlatform
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	FRotator rotatePertick = FRotator(0, 0.5, 0);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};
