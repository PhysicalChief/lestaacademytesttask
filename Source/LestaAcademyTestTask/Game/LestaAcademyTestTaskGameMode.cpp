// Copyright Epic Games, Inc. All Rights Reserved.

#include "../Game/LestaAcademyTestTaskGameMode.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALestaAcademyTestTaskGameMode::ALestaAcademyTestTaskGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}