// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "WindPlatfrom.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API AWindPlatfrom : public AStandartPlatformWithOverlapBox
{
	GENERATED_BODY()
	
public: 
	AWindPlatfrom();

	UPROPERTY()
	FTimerHandle changeDirectionTimer;

	UPROPERTY(EditDefaultsOnly)
	FVector windDirection = FVector(1, 1, 0);

	UPROPERTY(EditAnywhere)
	float windPowerCoef = 1;

	UPROPERTY(EditAnywhere)
	float moveRate = 0.005f;

	UPROPERTY(BlueprintReadWrite)
	TMap<class ALestaAcademyTestTaskCharacter*, FTimerHandle> characterMovable;

	UPROPERTY(EditAnywhere)
	UParticleSystemComponent* windParticles;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) override;

	virtual void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	void MoveCharacterOnBlock(ALestaAcademyTestTaskCharacter* character);

	UFUNCTION(BlueprintCallable)
	void changeWindDirection();
};
