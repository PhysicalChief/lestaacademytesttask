// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatform.h"
#include "StandartPlatformWithOverlapBox.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API AStandartPlatformWithOverlapBox : public AStandartPlatform
{
	GENERATED_BODY()

public:
	AStandartPlatformWithOverlapBox();

	UPROPERTY(EditDefaultsOnly)
	class UBoxComponent* boxOverlap;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

	UFUNCTION(BlueprintCallable)
	virtual void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
