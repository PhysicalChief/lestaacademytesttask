// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "LestaAcademyTestTaskGameInstance.generated.h"

UENUM(BlueprintType)
enum class EGameState : uint8 {
	EMainMenu,
	EWinGame,
	ELoseGame
};

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API ULestaAcademyTestTaskGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	EGameState gameState = EGameState::EMainMenu;

	UPROPERTY(BlueprintReadWrite)
	float levelTime;

	UPROPERTY(BlueprintReadWrite)
	bool sucsessFinished = false;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StartTimerLevel();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StopTimerLevel();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StartGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void StopGame();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PauseGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void QuitGame();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ResetDataInstance();
};
