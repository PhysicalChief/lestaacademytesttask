// Fill out your copyright notice in the Description page of Project Settings.


#include "../UtilActors/KillZone.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "../Game/LestaAcademyTestTaskGameInstance.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"

// Sets default values
AKillZone::AKillZone()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	defaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	SetRootComponent(defaultRoot);

	boxOverlap = CreateDefaultSubobject<UBoxComponent>(TEXT("KillZoneBoxComponent"));
	boxOverlap->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	boxOverlap->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	boxOverlap->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	boxOverlap->AttachTo(GetRootComponent());
}

// Called when the game starts or when spawned
void AKillZone::BeginPlay()
{
	Super::BeginPlay();

	boxOverlap->OnComponentBeginOverlap.AddDynamic(this, &AKillZone::OnBoxBeginOverlap);
}

// Called every frame
void AKillZone::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AKillZone::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* character = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (character) {
		ULestaAcademyTestTaskGameInstance* instance = Cast<ULestaAcademyTestTaskGameInstance>(GetGameInstance());
		if (instance) {
			instance->gameState = EGameState::ELoseGame;
			instance->sucsessFinished = false;
			instance->StopGame();
		}
	}
}

