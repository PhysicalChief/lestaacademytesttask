// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/BurnPlatform.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"

ABurnPlatform::ABurnPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	fireParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("fireParticleSystemComponent"));
	fireParticleSystemComponent->AttachTo(platformMeshComponent);
}

void ABurnPlatform::BeginPlay()
{
	Super::BeginPlay();
}

void ABurnPlatform::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* character = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (character) {
		FTimerDelegate delegate = FTimerDelegate::CreateUObject(this, &ABurnPlatform::burnCharacter, character);
		targetsTimers.Add(TTuple<ALestaAcademyTestTaskCharacter*, FTimerHandle>(character, FTimerHandle()));
		GetWorld()->GetTimerManager().SetTimer(targetsTimers[character], delegate, tickDamage, true, lagDamage);
	}
}

void ABurnPlatform::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ALestaAcademyTestTaskCharacter* character = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (character) {
		GetWorld()->GetTimerManager().ClearTimer(targetsTimers[character]);
		targetsTimers.Remove(character);
	}
}

void ABurnPlatform::burnCharacter(ALestaAcademyTestTaskCharacter* character)
{
	character->Damage(platformDamage);
}
