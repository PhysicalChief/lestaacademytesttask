// Fill out your copyright notice in the Description page of Project Settings.


#include "../UtilActors/FinishActor.h"
#include "../Game/LestaAcademyTestTaskGameInstance.h"
#include "Engine/Classes/Components/BoxComponent.h"

// Sets default values
AFinishActor::AFinishActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	defaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	SetRootComponent(defaultRoot);

	stopMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StartMeshComponent"));
	stopMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	stopMeshComponent->bEditableWhenInherited = true;
	stopMeshComponent->AttachTo(GetRootComponent());

	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("StartBoxCollisionComponent"));
	boxComponent->AttachTo(GetRootComponent());
	boxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	boxComponent->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	boxComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
}

// Called when the game starts or when spawned
void AFinishActor::BeginPlay()
{
	Super::BeginPlay();
	
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AFinishActor::OnBoxBeginOverlap);
}

// Called every frame
void AFinishActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFinishActor::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ULestaAcademyTestTaskGameInstance* currentGameInstance = Cast<ULestaAcademyTestTaskGameInstance>(GetGameInstance());
	if (currentGameInstance) {
		currentGameInstance->StopTimerLevel();
		currentGameInstance->gameState = EGameState::EWinGame;
		currentGameInstance->StopGame();
	}
}

