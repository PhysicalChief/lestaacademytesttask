// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StandartPlatform.generated.h"

UCLASS()
class LESTAACADEMYTESTTASK_API AStandartPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStandartPlatform();

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* defaultRoot;

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* platformMeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
