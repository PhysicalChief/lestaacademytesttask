// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FinishActor.generated.h"

UCLASS()
class LESTAACADEMYTESTTASK_API AFinishActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFinishActor();

	UPROPERTY(EditDefaultsOnly)
	USceneComponent* defaultRoot;

	UPROPERTY(EditDefaultsOnly)
	class UStaticMeshComponent* stopMeshComponent;

	UPROPERTY(EditDefaultsOnly)
	class UBoxComponent* boxComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

	UFUNCTION(BlueprintImplementableEvent)
	void endGameLevel();
};
