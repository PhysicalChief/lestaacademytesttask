// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LestaAcademyTestTaskGameMode.generated.h"

UCLASS(minimalapi)
class ALestaAcademyTestTaskGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALestaAcademyTestTaskGameMode();

};



