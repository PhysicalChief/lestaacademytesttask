// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "JumpPlatform.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API AJumpPlatform : public AStandartPlatformWithOverlapBox
{
	GENERATED_BODY()
	
public:
	AJumpPlatform();

	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* pushPlatformMeshComponent;

	UPROPERTY(EditDefaultsOnly)
	class UAudioComponent* jumpSoundComponent;

	UPROPERTY()
	FTimerHandle animHandle;

	UPROPERTY(EditAnywhere)
	float animFramerate = 0.003f;

	UPROPERTY(EditAnywhere)
	float animHight = 100.0f;

	UPROPERTY(EditAnywhere)
	float jumpHight = 1500.0f;

	UPROPERTY(EditAnywhere)
	FVector offsetPertick = FVector(0, 0, 5);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	float currentHight = 0;

	bool inAction = false;

	UPROPERTY()
	TSet<class ALestaAcademyTestTaskCharacter*> targets;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public: 
	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) override;

	virtual void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UFUNCTION(BlueprintCallable)
	void pushPlatform();

	UFUNCTION(BlueprintCallable)
	void launchTargets();

	UFUNCTION(BlueprintCallable)
	void rollBackPlatform();
};
