// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "DamagePlatform.generated.h"

class UMaterial;
class UBoxComponent;
class ALestaAcademyTestTaskCharacter;

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API ADamagePlatform : public AStandartPlatformWithOverlapBox
{
	GENERATED_BODY()

public:
	ADamagePlatform();

	UPROPERTY(EditAnywhere)
	int blockDamage = 20;

	UPROPERTY(BlueprintReadWrite)
	TArray<ALestaAcademyTestTaskCharacter*> damageTargets;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* standartMaterial;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* triggeredMaterial;

	UPROPERTY(EditDefaultsOnly)
	UMaterial* damageMaterial;

	UPROPERTY()
	FTimerHandle timerEvent;

	UPROPERTY(BlueprintReadWrite)
	bool bEventIsStarted = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	void DamageTarget();

	UFUNCTION(BlueprintCallable)
	void StartBlockEvent();

	UFUNCTION(BlueprintCallable)
	void StopBlockEvent();

	UFUNCTION(BlueprintCallable)
	void ReloadBlockEvent();

	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) override;

	virtual void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;
};
