// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/WindPlatfrom.h"
#include "WindPlatfrom.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"
#include "Math/UnrealMathUtility.h"

AWindPlatfrom::AWindPlatfrom()
{
	PrimaryActorTick.bCanEverTick = true;

	windParticles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("windParticlesComponent"));
	windParticles->AttachTo(platformMeshComponent);

}

void AWindPlatfrom::BeginPlay()
{
	Super::BeginPlay();

	GetWorld()->GetTimerManager().SetTimer(changeDirectionTimer, this, &AWindPlatfrom::changeWindDirection, 2, true, 0);
	windParticles->SetWorldRotation(GetActorRotation());
}

void AWindPlatfrom::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AWindPlatfrom::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* newCharacterInBox = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (newCharacterInBox) {
		if (!characterMovable.Contains(newCharacterInBox)) {
			FTimerDelegate actorDelegate = FTimerDelegate::CreateUObject(this, &AWindPlatfrom::MoveCharacterOnBlock, newCharacterInBox);
			characterMovable.Add(TTuple<ALestaAcademyTestTaskCharacter*, FTimerHandle>(newCharacterInBox, FTimerHandle()));
			GetWorld()->GetTimerManager().SetTimer(characterMovable[newCharacterInBox], actorDelegate, moveRate, true, 0);
		}
	}
}

void AWindPlatfrom::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ALestaAcademyTestTaskCharacter* outCharacter = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (outCharacter) {
		if (characterMovable.Contains(outCharacter)) {
			FTimerHandle deletedHandler = characterMovable[outCharacter];
			GetWorld()->GetTimerManager().ClearTimer(deletedHandler); 
			characterMovable.Remove(outCharacter);
		}
	}
}

void AWindPlatfrom::MoveCharacterOnBlock(ALestaAcademyTestTaskCharacter* character)
{
	FHitResult* hitResult = new FHitResult;
	character->AddActorWorldOffset(windDirection * windPowerCoef, false, hitResult, ETeleportType::None);
	delete hitResult;
}

void AWindPlatfrom::changeWindDirection()
{
	float newX = 0, newY = 0;
	while (newX == 0 && newY == 0) {
		newX = FMath::RandRange(-1, 1);
		newY = FMath::RandRange(-1, 1);
	}
	windDirection = FVector(newX, newY, 0);
	windDirection.Normalize(1);
}

