// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/JumpPlatform.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"
#include "Components/AudioComponent.h"

AJumpPlatform::AJumpPlatform()
{
	PrimaryActorTick.bCanEverTick = true;

	pushPlatformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PushPlatformMeshComponent"));
	pushPlatformMeshComponent->AttachTo(platformMeshComponent);
	boxOverlap->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	boxOverlap->AttachTo(platformMeshComponent);

	jumpSoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("jumpSoundComponent"));
	jumpSoundComponent->AttachTo(GetRootComponent());
	jumpSoundComponent->bAutoActivate = false;
}

void AJumpPlatform::BeginPlay()
{
	Super::BeginPlay();
}

void AJumpPlatform::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* newCharacterInBox = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (newCharacterInBox) {
		targets.Add(newCharacterInBox);
		launchTargets();
		if (inAction) return;
		inAction = true;
		FTimerDelegate actorDelegate = FTimerDelegate::CreateUObject(this, &AJumpPlatform::pushPlatform);
		GetWorld()->GetTimerManager().SetTimer(animHandle, actorDelegate, animFramerate, true, 0.005);
	}
}

void AJumpPlatform::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AJumpPlatform::pushPlatform()
{
	if (currentHight < animHight) {
		pushPlatformMeshComponent->AddWorldOffset(offsetPertick);
		currentHight += offsetPertick.Z;
	}
	else {
		GetWorld()->GetTimerManager().ClearTimer(animHandle);
		GetWorld()->GetTimerManager().SetTimer(animHandle, this, &AJumpPlatform::rollBackPlatform, animFramerate, true, 0.0f);
	}
}

void AJumpPlatform::launchTargets()
{
	jumpSoundComponent->Activate(false);
	for (auto target : targets) {
		target->LaunchCharacter(FVector(0, 0, jumpHight), false, true);
	}
	targets.Reset();
}

void AJumpPlatform::rollBackPlatform()
{
	if (currentHight > 0) {
		pushPlatformMeshComponent->AddWorldOffset(offsetPertick * -0.5);
		currentHight -= offsetPertick.Z * 0.5;
	}
	else {
		GetWorld()->GetTimerManager().ClearTimer(animHandle);
		inAction = false;
	}
}
