// Copyright Epic Games, Inc. All Rights Reserved.

#include "LestaAcademyTestTaskCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "../Game/LestaAcademyTestTaskGameMode.h"
#include "../Game/LestaAcademyTestTaskGameInstance.h"
#include "Components/PostProcessComponent.h"

//////////////////////////////////////////////////////////////////////////
// ALestaAcademyTestTaskCharacter

ALestaAcademyTestTaskCharacter::ALestaAcademyTestTaskCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	postprcessComponent = CreateDefaultSubobject<UPostProcessComponent>(TEXT("PostproccessDamage"));
	postprcessComponent->SetupAttachment(GetRootComponent());

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

}

//////////////////////////////////////////////////////////////////////////
// Input

void ALestaAcademyTestTaskCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ALestaAcademyTestTaskCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ALestaAcademyTestTaskCharacter::StopJump);

	PlayerInputComponent->BindAxis("MoveForward", this, &ALestaAcademyTestTaskCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ALestaAcademyTestTaskCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ALestaAcademyTestTaskCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ALestaAcademyTestTaskCharacter::LookUpAtRate);

	// handle touch devices
	PlayerInputComponent->BindTouch(IE_Pressed, this, &ALestaAcademyTestTaskCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &ALestaAcademyTestTaskCharacter::TouchStopped);

	// VR headset functionality
	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &ALestaAcademyTestTaskCharacter::OnResetVR);
	PlayerInputComponent->BindAction("PauseGame", IE_Pressed, this, &ALestaAcademyTestTaskCharacter::PauseGame);

	health = maxHealth;

	postprcessComponent->bEnabled = false;
}

void ALestaAcademyTestTaskCharacter::Damage(int damageValue)
{
	health -= damageValue;
	UpdateHealth();
	postprcessComponent->bEnabled = true;
	FTimerHandle handle;
	GetWorld()->GetTimerManager().SetTimer(handle, this, &ALestaAcademyTestTaskCharacter::DisablePostprocess, 1, false, 1.3f);

	if (health <= 0) {
		ULestaAcademyTestTaskGameInstance* currentGameInstance = Cast<ULestaAcademyTestTaskGameInstance>(GetWorld()->GetGameInstance());
		if (currentGameInstance) {
			currentGameInstance->gameState = EGameState::ELoseGame;
			currentGameInstance->StopGame();
		}
	}
}

void ALestaAcademyTestTaskCharacter::DisablePostprocess()
{
	postprcessComponent->bEnabled = false;
}


void ALestaAcademyTestTaskCharacter::OnResetVR()
{
	// If LestaAcademyTestTask is added to a project via 'Add Feature' in the Unreal Editor the dependency on HeadMountedDisplay in LestaAcademyTestTask.Build.cs is not automatically propagated
	// and a linker error will result.
	// You will need to either:
	//		Add "HeadMountedDisplay" to [YourProject].Build.cs PublicDependencyModuleNames in order to build successfully (appropriate if supporting VR).
	// or:
	//		Comment or delete the call to ResetOrientationAndPosition below (appropriate if not supporting VR)
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void ALestaAcademyTestTaskCharacter::TouchStarted(ETouchIndex::Type FingerIndex, FVector Location)
{
		Jump();
}

void ALestaAcademyTestTaskCharacter::TouchStopped(ETouchIndex::Type FingerIndex, FVector Location)
{
		StopJumping();
}

void ALestaAcademyTestTaskCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ALestaAcademyTestTaskCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ALestaAcademyTestTaskCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ALestaAcademyTestTaskCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void ALestaAcademyTestTaskCharacter::Jump()
{
	bPressedJump = true;
	JumpKeyHoldTime = 0.0f;
}

void ALestaAcademyTestTaskCharacter::StopJump()
{
	bPressedJump = false;
	ResetJumpState();
}

void ALestaAcademyTestTaskCharacter::PauseGame()
{
	ULestaAcademyTestTaskGameInstance* currentGameInstance = Cast<ULestaAcademyTestTaskGameInstance>(GetWorld()->GetGameInstance());
	if (currentGameInstance) {
		currentGameInstance->PauseGame();
	}
}
