// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/FadePlatform.h"
#include "FadePlatform.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"

AFadePlatform::AFadePlatform()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFadePlatform::BeginPlay()
{
	Super::BeginPlay();

	defultScale = platformMeshComponent->GetRelativeScale3D();
	currentScale = defultScale;
}

void AFadePlatform::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* character = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (character) {
		GetWorld()->GetTimerManager().SetTimer(actionTimer, this, &AFadePlatform::fadePlatformScale, fadeTimeStep, true, fadeTimeStep);
	}
}

void AFadePlatform::fadePlatformScale()
{

	if (currentScale.X > 0 && currentScale.Y > 0) {
		currentScale.X -= fadeScaleStepX;
		currentScale.Y -= fadeScaleStepY;
		platformMeshComponent->SetRelativeScale3D(currentScale);
	}
	if (currentScale.X <= 0.05f && currentScale.Y <= 0.05f) {
		platformMeshComponent->SetRelativeScale3D(FVector(0, 0, 0));
		platformMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		GetWorld()->GetTimerManager().SetTimer(actionTimer, this, &AFadePlatform::reloadPlatformScale, fadeTimeStep, false, fadeTimeStep);
	}
}

void AFadePlatform::reloadPlatformScale()
{
	platformMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	platformMeshComponent->SetRelativeScale3D(defultScale);
}

