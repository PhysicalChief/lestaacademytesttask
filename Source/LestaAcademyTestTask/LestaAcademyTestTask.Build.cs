// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class LestaAcademyTestTask : ModuleRules
{
	public LestaAcademyTestTask(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
