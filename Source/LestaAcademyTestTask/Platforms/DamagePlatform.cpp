// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/DamagePlatform.h"
#include "../Characters/LestaAcademyTestTaskCharacter.h"
#include "Engine/Classes/Components/BoxComponent.h"

ADamagePlatform::ADamagePlatform() {
	PrimaryActorTick.bCanEverTick = true;

	standartMaterial = CreateDefaultSubobject<UMaterial>(TEXT("StandartMaterial"));
	triggeredMaterial = CreateDefaultSubobject<UMaterial>(TEXT("TriggeredMaterial"));
	damageMaterial = CreateDefaultSubobject<UMaterial>(TEXT("DamageMaterial"));
}

void ADamagePlatform::BeginPlay()
{
	Super::BeginPlay();


}

void ADamagePlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ADamagePlatform::DamageTarget()
{
	platformMeshComponent->SetMaterial(0, damageMaterial);
	GetWorld()->GetTimerManager().SetTimer(timerEvent, this, &ADamagePlatform::StopBlockEvent, 0.3, false, 0.3);
	if (damageTargets.Num() > 0) {
		for (int i = 0; i < damageTargets.Num(); i++) {
			damageTargets[i]->Damage(blockDamage);
		}
	}
}

void ADamagePlatform::StartBlockEvent()
{	
	if (bEventIsStarted) return;
	bEventIsStarted = true;
	platformMeshComponent->SetMaterial(0, triggeredMaterial);
 	GetWorld()->GetTimerManager().SetTimer(timerEvent, this, &ADamagePlatform::DamageTarget, 1, false, 1);
}

void ADamagePlatform::StopBlockEvent()
{
	platformMeshComponent->SetMaterial(0, standartMaterial);
	GetWorld()->GetTimerManager().SetTimer(timerEvent, this, &ADamagePlatform::ReloadBlockEvent, 5, false, 5);
}

void ADamagePlatform::ReloadBlockEvent()
{
	bEventIsStarted = false;
	if (damageTargets.Num() > 0) {
		StartBlockEvent();
	}
}

void ADamagePlatform::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALestaAcademyTestTaskCharacter* target = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (target) {
		damageTargets.Add(target);
	}
	StartBlockEvent();
}

void ADamagePlatform::OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ALestaAcademyTestTaskCharacter* target = Cast<ALestaAcademyTestTaskCharacter>(OtherActor);
	if (target) {
		damageTargets.Remove(target);
	}
}
