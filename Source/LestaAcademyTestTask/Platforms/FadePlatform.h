// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "FadePlatform.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API AFadePlatform : public AStandartPlatformWithOverlapBox
{
	GENERATED_BODY()
	
public:
	AFadePlatform();

	FTimerHandle actionTimer;

	UPROPERTY(BlueprintReadWrite)
	FVector defultScale;

	UPROPERTY(EditDefaultsOnly)
	FVector currentScale;

	UPROPERTY(EditDefaultsOnly)
	float fadeScaleStepX = 0.2f;

	UPROPERTY(EditDefaultsOnly)
	float fadeScaleStepY = 0.2f;

	UPROPERTY(EditDefaultsOnly)
	float fadeTimeStep = 1.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) override;

	void fadePlatformScale();

	void reloadPlatformScale();

};
