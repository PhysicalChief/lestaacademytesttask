// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Platforms/StandartPlatformWithOverlapBox.h"
#include "BurnPlatform.generated.h"

/**
 * 
 */
UCLASS()
class LESTAACADEMYTESTTASK_API ABurnPlatform : public AStandartPlatformWithOverlapBox
{
	GENERATED_BODY()

public:
	ABurnPlatform();

	UPROPERTY(EditDefaultsOnly)
	UParticleSystemComponent* fireParticleSystemComponent;

	UPROPERTY(BlueprintReadWrite)
	TMap<class ALestaAcademyTestTaskCharacter*, FTimerHandle> targetsTimers;

	UPROPERTY(EditDefaultsOnly)
	float platformDamage = 5;

	UPROPERTY(EditDefaultsOnly)
	float tickDamage = 1;

	UPROPERTY(EditDefaultsOnly)
	float lagDamage = 2;

	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:

	virtual void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) override;

	virtual void OnBoxEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) override;

	UFUNCTION(BlueprintCallable)
	void burnCharacter(class ALestaAcademyTestTaskCharacter* character);
};
