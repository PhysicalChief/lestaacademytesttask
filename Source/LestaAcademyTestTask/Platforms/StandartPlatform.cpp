// Fill out your copyright notice in the Description page of Project Settings.


#include "../Platforms/StandartPlatform.h"

// Sets default values
AStandartPlatform::AStandartPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	defaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	SetRootComponent(defaultRoot);

	platformMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StandartPlatformMeshComponent"));
	platformMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	platformMeshComponent->bEditableWhenInherited = true;
	platformMeshComponent->AttachTo(GetRootComponent());

}

// Called when the game starts or when spawned
void AStandartPlatform::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStandartPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

