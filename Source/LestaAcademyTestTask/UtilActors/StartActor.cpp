// Fill out your copyright notice in the Description page of Project Settings.


#include "../UtilActors/StartActor.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "../Game/LestaAcademyTestTaskGameInstance.h"

// Sets default values
AStartActor::AStartActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	defaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	SetRootComponent(defaultRoot);

	startMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StartMeshComponent"));
	startMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	startMeshComponent->bEditableWhenInherited = true;
	startMeshComponent->AttachTo(GetRootComponent());

	boxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("StartBoxCollisionComponent"));
	boxComponent->AttachTo(GetRootComponent());
	boxComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	boxComponent->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	boxComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

}

// Called when the game starts or when spawned
void AStartActor::BeginPlay()
{
	Super::BeginPlay();
	
	boxComponent->OnComponentBeginOverlap.AddDynamic(this, &AStartActor::OnBoxBeginOverlap);
}

void AStartActor::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ULestaAcademyTestTaskGameInstance* currentGameInstance = Cast<ULestaAcademyTestTaskGameInstance>(GetGameInstance());
	if (currentGameInstance) {
		currentGameInstance->StartTimerLevel();
	}
}


// Called every frame
void AStartActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

